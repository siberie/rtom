VENV=python3 -m venv
VENV_DIR=venv
VENV_ACTIVATE=$(VENV_DIR)/bin/activate
REQ_FILE=requirements.txt


install:
	pip install .

setup:
	$(VENV) $(VENV_DIR)
	(\
		. $(VENV_ACTIVATE); \
		pip install -r $(REQ_FILE); \
	)

uninstall:
	pip uninstall rtom

clean:
	rm -r venv