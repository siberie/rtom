from setuptools import setup, find_packages

setup(name='rtom',
      version='0.2.1',
      description='RethinkDB Objective Model',
      url='',
      author='Tomek Stachowiak',
      author_email='tjstachowiak@gmail.com',
      license='MIT',
      packages=find_packages('./'),
      zip_safe=False)