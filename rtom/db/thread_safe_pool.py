import logging
from queue import Queue, Empty

logger = logging.getLogger('database')
logger.addHandler(logging.StreamHandler())


class ThreadSafePool(object):
    class PoolEmpty(Exception):
        pass

    class PoolCorrupted(Exception):
        pass

    def __init__(self, max_connections=20, pool_recycle=False, new_connection=None):
        self.free_connections = Queue(maxsize=max_connections)
        self.max_connections = max_connections
        self.pool_recycle = pool_recycle
        self.new_connection = new_connection
        for i in range(max_connections):
            self.free_connections.put(self.new_connection())

    def get(self):
        try:
            if self.pool_recycle:
                self._recycle_pool()

            connection = self.free_connections.get(block=True, timeout=5)
        except Empty:
            logger.warning('WARNING: Database connection pool is empty')
            raise ThreadSafePool.PoolEmpty('connection pool is empty')
        except Exception as e:
            raise ThreadSafePool.PoolCorrupted(str(e))
        else:
            pool = self

            class Connection(object):
                open = True

                def __call__(self):
                    return connection

                def close(self):
                    if self.open:
                        self.open = False
                        pool.free_connections.put(connection)

                def __enter__(self): return connection

                def __exit__(self, *_): self.close()

            return Connection()

# TODO pool recycling
    def _recycle_pool(self):
        logger.debug('Recycling the pool')
        temp_pool = []

        while not self.free_connections.empty():
            conn = self.free_connections.get(block=False)
            if conn is None:
                conn = self.new_connection
            elif not conn.is_open():
                conn.reconnect()
            temp_pool.append(conn)

        for item in temp_pool:
            self.free_connections.put(item)
