import rethinkdb as r
import logging

logger = logging.getLogger('db')
logger.setLevel(logging.DEBUG)


def connect(hostname, port, database=None):
    try:
        conn = r.connect(host=hostname, port=port)
        if database is not None:
            conn.use(db=database)
    except r.ReqlDriverError as e:
        logger.exception('Database connection error: ' + e.message)
        return None
    return conn


def table_list(connection):
    return r.table_list().run(connection)


def table_create(connection, name):
    return r.table_create(name).run(connection)


def table_reset(connection, name):
    return r.table(name).delete().run(connection)


def table_drop(connection, name):
    return r.table_drop(name).run(connection)


def table_clean(connection, name):
    r.table(name).delete().run(connection)


def database_create(connection, name, force=False):
    if force:
        try:
            r.db_drop(name).run(connection)
        except r.RqlRuntimeError:
            pass
    try:
        return r.db_create(name).run(connection)
    except r.errors.ReqlOpFailedError:
        print('database %s exists... skipping' % name)
        return None


def database_drop(connection, name):
    return r.db_drop(name).run(connection)


def database_use(connection, name):
    connection.use(db=name)
