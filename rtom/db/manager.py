from .thread_safe_pool import ThreadSafePool
from . import driver as default_driver


class DBManager:
    model_tables = {}

    def __init__(self, host, port, database, driver=default_driver):
        self.driver = driver
        self.host = host
        self.port = port
        self.database = database
        self.pool = ThreadSafePool(max_connections=10, new_connection=self.__create_connection)

    def register(self, table_name, model):
        self.model_tables[model] = table_name

    def sync(self, remove_unknown=True):
        tables = set(self.tables)
        model_tables = set(self.model_tables.values())
        old_tables = tables - model_tables
        new_tables = model_tables - tables

        self.create_tables(list(new_tables))
        if remove_unknown:
            self.drop_tables(list(old_tables))

    @property
    def tables(self):
        with self.connection as conn:
            return self.driver.table_list(conn)

    @property
    def connection(self):
        return self.pool.get()

    def create_database(self, name, force=False):
        conn = self.driver.connect(self.host, self.port)
        self.driver.database_create(conn, name, force=force)

    def drop_database(self):
        with self.connection as conn:
            self.driver.database_drop(conn, self.database)

    def reset_database(self):
        self.reset_tables(self.tables)

    def create_tables(self, tables):
        with self.connection as conn:
            for table in tables:
                self.driver.table_create(conn, table)

    def reset_tables(self, tables):
        with self.connection as conn:
            for table in tables:
                self.driver.table_reset(conn, table)

    def drop_tables(self, tables):
        with self.connection as conn:
            for table in tables:
                self.driver.table_drop(conn, table)

    def __create_connection(self):
        """
        _internal only_: use self.pool.get() instead
        """
        return self.driver.connect(self.host, self.port, self.database)
