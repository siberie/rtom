from . import default_manager


def bind_table(table_name, manager=default_manager):
    def bind(model_class):
        setattr(model_class, 'db_manager', manager)
        manager.register(table_name, model_class)
        return model_class
    return bind
