def isiterable(x):
    try:
        iter(x)
    except TypeError:
        return False
    return True


class Field:
    default = None

    def __init__(self, required=True, default=None):
        self.name = None
        self.is_required = required
        self.default = default

    def bind(self, cls, name):
        self.name = name

    def init(self, instance):
        instance.set(self.name, default)

    def __get__(self, instance, owner):
        return instance.get(self.name)

    def __set__(self, instance, value):
        instance.set(self.name, value)


class TypedField(Field):
    type = type

    def __set__(self, instance, value):
        if not isinstance(value, self.type):
            value = self.type(value)
        super(TypedField, self).__set__(instance, value)


class StringField(TypedField):
    type = str


class IntField(TypedField):
    type = int


class ListField(Field):
    def __init__(self, type=None, **kwargs):
        self.type = type
        super(ListField, self).__init__(**kwargs)

    def __set__(self, instance, value):
        if not isinstance(value, list) and not isiterable(value):
            raise TypeError('list or iterable expected but found %s' % str(type(value)))
        if self.type is not None:
            for x in value:
                if not isinstance(x, self.type):
                    raise TypeError('wrong element type: %s expected, %s found' % (str(self.type), str(type(value))))
                # TODO: this won't work for list.append() calls

        super(ListField, self).__set__(instance, value)


class ModelField(Field):
    def __init__(self, model_class, **kwargs):
        super(ModelField, self).__init__(**kwargs)
        self.model_class = model_class
        self.model = self.model_class()

    def __set__(self, instance, value):
        if isinstance(value, self.model_class):
            self.model = value
        elif isinstance(value, dict):
            self.model = self.model_class(**value)
        else:
            raise TypeError()

        instance.set(self.name, self.model.__dict__)

    def __get__(self, instance, owner):
        self.model.__dict__ = instance.get(self.name, dict())
        return self.model


class DatetimeField(StringField):
    pass
