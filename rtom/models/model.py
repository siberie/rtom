from .builder import ModelBuilder


class MetaModel(type):
    _fields = None  # type: dict

    def __new__(mcs, name, bases, attrs: dict):
        super_new = super().__new__
        parents = [b for b in bases if isinstance(b, MetaModel)]
        if not any(parents):
            return super_new(mcs, name, bases, attrs)

        module = attrs.pop('__module__', None)
        b = ModelBuilder(super_new(mcs, name, bases, {'__module__': module, 'asset_name': name}))

        for name, attr in attrs.items():
            b.add(name, attr)

        return b.to_model()

    def __contains__(self, item):
        return item in self._fields


class Model(metaclass=MetaModel):
    manager = None

    class FieldDoesNotExist(Exception):
        pass

    def __init__(self, **values):
        for name, field in self._fields.items():
            self.set(name, values[name] if name in values else field.default)

        super().__init__()

    def set(self, name, value):
        self.__dict__[name] = value

    def get(self, name, default=None):
        if name not in self.__dict__ and default is not None:
            self.set(name, default)

        return self.__dict__.get(name, default)

    def update(self, data):
        for k, v in data.items():
            if k not in self._fields:
                raise Model.FieldDoesNotExist(k)
            else:
                self.set(k, v)

    def save(self):
        self.manager.save()

    def delete(self):
        self.manager.delete()

    def to_dict(self):
        # TODO: _fields out of blue - fix! btw: it's set by the ModelBuilder
        print(self.__dict__)
        return self.__dict__;
        # return dict((name, self.get(name)) for name in self._fields.keys() if self.get(name) is not None)

    @classmethod
    def from_dict(cls, d: dict):
        return cls(**d)
