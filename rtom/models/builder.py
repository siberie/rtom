from inspect import isclass

from .manager import ModelManager
from .fields import Field, StringField


class ModelBuilder:
    def __init__(self, obj):
        self.__model = obj
        self.manager_name = None

        setattr(self.__model, '_fields', {})

    def add(self, name, obj):
        if not isclass(obj) and hasattr(obj, 'bind'):
            obj.bind(self.__model, name)

        if isinstance(obj, ModelManager):
            self.manager_name = name
            if obj.default:
                self.__model.manager = obj
            elif self.__model.manager is None:
                self.__model.manager = obj

        setattr(self.__model, name, obj)

        if isinstance(obj, Field):
            self.__model._fields[name] = obj

        return ModelBuilder

    def to_model(self):
        if id not in self.__model._fields:
            self.add('id', StringField(required=True))
        if not self.manager_name:
            self.add('objects', ModelManager(default=True))

        return self.__model

    def set(self, name, value):
        self.__model.set(name, value)
