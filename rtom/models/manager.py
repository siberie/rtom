from ..query.builder import QueryBuilder


class ModelManager:
    def __init__(self, default=False):
        self.name = None
        self.default = default
        self.model_class = None
        self.model = None

    def bind(self, cls, name):
        self.name = name
        self.model_class = cls

    def __get__(self, instance, owner):
        self.model = instance
        return QueryBuilder(self)
