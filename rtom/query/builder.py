import rethinkdb as r
from .query_set import QuerySet


class QueryBuilder:
    class FieldDoesNotExist(Exception):
        pass
    class OperationError(Exception):
        pass

    def __init__(self, manager):
        self.model = manager.model
        self.model_class = manager.model_class
        self.table = self.model_class.db_manager.model_tables[manager.model_class]
        self._filter = {}
        self.query = r.table(self.table)

    def filter(self, **kwargs):
        if not all(field in self.model_class for field in kwargs.keys()):
            raise QueryBuilder.FieldDoesNotExist('unknown field in filter')
        self._filter.update(kwargs)
        self.query = self.query.filter(kwargs)
        return self

    def all(self):
        with self.model_class.db_manager.connection as conn:
            return QuerySet(model_class=self.model_class, data=self.query.run(conn))

    def get(self, **kwargs):
        print('getting', kwargs)
        with self.model_class.db_manager.connection as conn:
            if 'id' in kwargs:
                res = self.query.get(kwargs['id']).run(conn)
                return self.model_class(**res)

    def delete(self):
        if self.model.id is None or self.model.id == '':
            raise QueryBuilder.OperationError('cannot delete an object without "id"')

        with self.model_class.db_manager.connection as conn:
            return self.query.get(self.model.id).delete().run(conn)

    def save(self):
        if self.model.id is None or self.model.id == '':
            d = self.model.to_dict()
            if 'id' in d:
                del d['id']
            with self.model_class.db_manager.connection as conn:
                res = self.query.insert(d).run(conn)
                if 'generated_keys' in res and len(res['generated_keys']) > 0:
                    self.model.id = res['generated_keys'].pop()
        else:
            with self.model_class.db_manager.connection as conn:
                self.query.get(self.model.id).update(self.model.to_dict()).run(conn)
