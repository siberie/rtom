class QuerySet:
    data = []
    _model_cache = []
    model_class = None

    def __init__(self, model_class, data):
        self.data = data
        self.model_class = model_class

    class QuerySetEmpty(Exception):
        pass

    def as_dicts(self):
        return self.data

    def as_models(self):
        if len(self._model_cache) != len(self.data):
            self._model_cache = [self.model_class(**item) for item in self.data]

        return self._model_cache

    def __iter__(self):
        return iter(self.data)

    @property
    def last(self):
        self._raise_if_empty()
        return self.data[-1]

    @property
    def first(self):
        self._raise_if_empty()
        return self.data[0]

    def _raise_if_empty(self):
        if len(self.data) == 0:
            raise self.QuerySetEmpty()