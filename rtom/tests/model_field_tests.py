import unittest as ut

import rtom.models as models
from db.decorators import bind_table
from rtom.db import DBManager


class ModelTest(ut.TestCase):
    def setUp(self):
        self.manager = DBManager('localhost', 28015, 'unittest')
        self.manager.create_database('unittest', force=True)
        self.manager.sync()

        class TestModelA(models.Model):
            x = models.StringField()
            y = models.IntField()

        @bind_table('test_model_b', self.manager)
        class TestModelB(models.Model):
            a = models.ModelField(TestModelA)
            x = models.IntField()

        self.modelA = TestModelA
        self.modelB = TestModelB
        self.test_data = {
            'x': 1,
            'a': {'x': '2', 'y': 3}
        }

    def test_deserialization(self):
        test = self.modelB(**self.test_data)
        self.assertEqual(self.test_data['x'], test.x)
        self.assertEqual(self.test_data['a']['x'], test.a.x)
        self.assertEqual(self.test_data['a']['y'], test.a.y)
        self.assertDictEqual(test.__dict__, self.test_data)

    def test_save_and_load(self):
        test = self.modelB(**self.test_data)
        test.save()
        self.assertIsNotNone(test.id)

        test2 = self.modelB.objects.get(id=test.id)
        self.assertDictEqual(test.__dict__, test2.__dict__)
