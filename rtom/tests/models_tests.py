import unittest as ut

import rtom.models as models


class ModelTest(ut.TestCase):
    def test_empty(self):
        class EmptyModel(models.Model):
            pass

        self.assertIsNotNone(EmptyModel())

    def test_with_string(self):
        class StringModel(models.Model):
            string = models.StringField()

        model = StringModel()
        self.assertIsNotNone(model)
        self.assertTrue(hasattr(model, 'string'))

        model.string = 'test value'

        self.assertEqual('test value', model.string)

    def test_2_models(self):
        class TestModel(models.Model):
            string = models.StringField()

        model1 = TestModel()
        model2 = TestModel()
        model1.string = 'test value 1'
        model2.string = 'test value 2'

        self.assertEqual('test value 1', model1.string)
        self.assertEqual('test value 2', model2.string)

    def test_model_to_dict(self):
        class TestModel(models.Model):
            string1 = models.StringField()
            int1 = models.IntField()

        test = TestModel()
        test.string1 = 'string1'
        test.int1 = 1
        self.assertDictEqual({'string1': 'string1', 'int1': 1}, test.to_dict())

    def test_model_from_dict(self):
        class TestModel(models.Model):
            string1 = models.StringField()
            int2 = models.IntField()

        d = {'string1': 'string1', 'int2': 2}
        test = TestModel.from_dict(d)
        self.assertDictEqual(d, test.to_dict())


if __name__ == '__main__':
    ut.main()
