import unittest as ut

from db.decorators import bind_table
from rtom.db import DBManager
from rtom.models import Model, StringField, IntField


class ManagerTests(ut.TestCase):
    def setUp(self):
        self.manager = DBManager('localhost', 28015, 'unittest')
        self.manager.create_database('unittest', force=True)

    def takeDown(self):
        del self.manager

    def test_decorator(self):
        @bind_table('test_table', self.manager)
        class TestModel(Model):
            pass

        self.assertIsNotNone(TestModel())
        self.assertDictEqual({TestModel: 'test_table'}, self.manager.model_tables)

        self.manager.sync()

    def test_save(self):
        @bind_table('test_table', self.manager)
        class TestModel(Model):
            string1 = StringField()
            int1 = IntField()

        self.manager.sync()
        test = TestModel(string1='string1', int1=1)
        test.save()

        objects = list(test.objects.all())

        self.assertEqual(1, len(objects))
        self.assertDictEqual(test.to_dict(), objects[0])


if __name__ == '__main__':
    ut.main()
