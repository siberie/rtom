from sys import argv

from rtom import default_manager

help_string = \
    """rtom control v.2017.3
    commands:
        createdatabase <name>: create database 'name' using default connection
        sync                 : sync database
    """


def get_switches(v: [str]):
    switches = {}
    not_switches = []
    for x in v:
        if x.startswith('--') and len(x) > 3:
            parts = x[2:].split('=')
            if len(parts) > 1:
                switches[parts[0]] = parts[1]
            else:
                switches[parts[0]] = True
        else:
            not_switches.append(x)

    return switches, not_switches


def print_help():
    print(help_string)


def get_command_and_params():
    if len(argv) == 1:
        print_help()
        return None, None, None
    cmd = argv[1]
    params = argv[2:]
    switches, params = get_switches(params)
    return cmd, switches, params


def command_createdatabase(switches, params):
    if len(params) == 0:
        print('no database name given')
        return
    if len(params) > 1:
        print('too many params')
        return

    assert len(params) <= 1

    force = 'force' in switches
    db_name = params[0]
    default_manager.create_database(db_name, force=force)


if __name__ == '__main__':
    cmd, switches, params = get_command_and_params()
    if cmd is None:
        print('no command. bailing out.')
        quit(0)

    cmd_decorated = 'command_' + cmd
    if cmd_decorated in globals():
        globals()[cmd_decorated](switches, params)
    else:
        print('no such command %s' % cmd)
        print_help()
